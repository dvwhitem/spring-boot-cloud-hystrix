package com.ch.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by vitaliy on 10/9/16.
 */
@Data
@NoArgsConstructor
public class Message {

    private String id;

    private String payload;

    @JsonProperty("throw_exception")
    private boolean throwException;

    @JsonProperty("delay_by")
    private int delayBy = 0;


    @JsonCreator
    public Message(@JsonProperty("id") String id,
                   @JsonProperty("payload") String payload,
                   @JsonProperty("throw_exception") boolean throwException,
                   @JsonProperty("delay_by") int delayBy) {
        this.id = id;
        this.payload = payload;
        this.throwException = throwException;
        this.delayBy = delayBy;
    }
}
