package com.ch.commands.collapsed;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by vitaliy on 10/9/16.
 */
public class PersonService {
    public Person findPerson(Integer id) {
        return new Person(id, "name : " + id);
    }

    public List<Person> findPeople(List<Integer> ids) {
        return ids
                .stream()
                .map(i -> new Person(i, "name : " + i))
                .collect(Collectors.toList());
    }
}
