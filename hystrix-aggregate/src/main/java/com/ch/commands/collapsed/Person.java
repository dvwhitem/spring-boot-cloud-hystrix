package com.ch.commands.collapsed;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

/**
 * Created by vitaliy on 10/9/16.
 */
@Data
@AllArgsConstructor
@ToString
public class Person {
    @Getter
    private  Integer id;
    @Getter
    private  String name;
}
