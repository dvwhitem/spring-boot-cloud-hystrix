package com.ch.controller;

import com.ch.commands.sample.HelloWorldCommand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 10/9/16.
 */
@RestController
public class HelloWorldController {
    @RequestMapping("/hello")
    public String callHelloWorldCommand(@RequestParam(value = "greeting", defaultValue = "World", required = false) String greeting) {
        HelloWorldCommand helloWorldCommand = new HelloWorldCommand(greeting);
        return helloWorldCommand.execute();
    }
}
