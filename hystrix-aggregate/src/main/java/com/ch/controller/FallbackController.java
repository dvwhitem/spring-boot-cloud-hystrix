package com.ch.controller;

import com.ch.commands.fallback.FallbackCommand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 10/9/16.
 */
@RestController
public class FallbackController {

    @RequestMapping("/fallback")
    public String callFallback() {
        FallbackCommand fallbackCommand = new FallbackCommand();
        return fallbackCommand.execute();
    }
}
