package com.ch.controller;

import com.ch.domain.Message;
import com.ch.domain.MessageAcknowledgement;
import com.ch.service.MessageHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * Created by vitaliy on 10/8/16.
 */
@RestController
public class MessageController {

    private MessageHandlerService messageHandlerService;

    @Autowired
    public MessageController(MessageHandlerService messageHandlerService) {
        this.messageHandlerService = messageHandlerService;
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public DeferredResult<MessageAcknowledgement> message(@RequestBody Message message) {
        DeferredResult<MessageAcknowledgement> deferred = new DeferredResult<>();
        messageHandlerService.handleMessage(message)
                .subscribe(m -> deferred.setResult(m), e-> deferred.setErrorResult(e));

        return deferred;
    }
}
