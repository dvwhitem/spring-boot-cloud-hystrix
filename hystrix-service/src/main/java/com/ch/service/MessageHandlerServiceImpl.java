package com.ch.service;

import com.ch.domain.Message;
import com.ch.domain.MessageAcknowledgement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.concurrent.TimeUnit;

/**
 * Created by vitaliy on 10/8/16.
 */
@Service
public class MessageHandlerServiceImpl implements MessageHandlerService{

    private static final Logger logger = LoggerFactory.getLogger(MessageHandlerServiceImpl.class);

    @Value("${reply.message}")
    private String replyMessage;

    @Override
    public Observable<MessageAcknowledgement> handleMessage(Message message) {
        logger.info("About Acknowledge");

        return Observable.timer(message.getDelayBy(), TimeUnit.MILLISECONDS)
                .map(l -> message.isThrowException())
                .map(throwException-> {
                    if(throwException) {
                        throw  new RuntimeException("Throwing an exception!");
                    }
                    return new MessageAcknowledgement(message.getId(), replyMessage, message.getPayload());
                });
    }
}
