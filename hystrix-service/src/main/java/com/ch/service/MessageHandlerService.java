package com.ch.service;

import com.ch.domain.Message;
import com.ch.domain.MessageAcknowledgement;
import rx.Observable;

/**
 * Created by vitaliy on 10/8/16.
 */
public interface MessageHandlerService {
    Observable<MessageAcknowledgement> handleMessage(Message message);
}
