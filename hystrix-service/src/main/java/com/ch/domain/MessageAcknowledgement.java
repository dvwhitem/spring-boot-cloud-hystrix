package com.ch.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;

/**
 * Created by vitaliy on 10/8/16.
 */
@Data
public class MessageAcknowledgement {

    @Getter
    private String id;
    @Getter
    private String received;
    @Getter
    private String payload;

    @JsonCreator
    public MessageAcknowledgement(@JsonProperty("id") String id,
                                  @JsonProperty("received") String received,
                                  @JsonProperty("payload") String payload) {
        this.id = id;
        this.received = received;
        this.payload = payload;
    }
}
