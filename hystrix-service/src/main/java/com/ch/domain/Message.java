package com.ch.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;

/**
 * Created by vitaliy on 10/8/16.
 */
@Data
public class Message {

    @Getter
    private  String id;
    @Getter
    private  String payload;

    @Getter
    @JsonProperty("throw_exception")
    private  boolean throwException;

    @Getter
    @JsonProperty("delay_by")
    private int delayBy;

    @JsonCreator
    public Message(@JsonProperty("id") String id,
                   @JsonProperty("payload") String payload,
                   @JsonProperty("throw_exception")  boolean throwException,
                   @JsonProperty("delay_by") int delayBy) {
        this.id = id;
        this.payload = payload;
        this.throwException = throwException;
        this.delayBy = delayBy;
    }
}
